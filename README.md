# py_simple

Tiny config

Sample of usage

#cration

config_name = 'sample.cfg'

try:
    config=Config(config_name)
except FileNotFoundError as e: # create default config if not exists
    config=Config()
    config['option_1'] = 'value_1'
    config['option_2'] = 140927157
    config.save_config(config_name)

# usage

option_1 = config['option_1']
option_2 = config['option_2']

# update
config['option_1'] = "value_'1"
config.save_config(config_name)