import os
from config import Config


def main():
    config_name = 'Eample_config.cfg'

    try:  # drop config if exists
        os.remove(config_name)
        print ("file", config_name, "is dropped!")
    except OSError:
        pass

    for on in ['creation', 'reading']:
        try:
            config = Config(config_name)
        except FileNotFoundError as e:
            config = Config()
            config['display_string'] = 'display_string'
            config['display_number'] = 111
            config['display_list'] = [2, 3, 4]
            config.save_config(config_name)

        print("Config on {}:".format(on),
              "display_string", config['display_string'],
              "display_number", config['display_number'],
              "display_list", config['display_list'])


if __name__ == "__main__":
    # execute only if run as a script
    main()
