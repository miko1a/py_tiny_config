
import json
import os

class Config(object):
    def __init__(self, js_path=None):
        self.prpts = {}
        if js_path:
            rd_prpts = ''
            with open(js_path, encoding='utf8') as prpfile:
                rd_prpts = prpfile.read()
            self.prpts = json.loads(rd_prpts)

    def __getattribute__(self, name):
        try:
            return super(Config, self).__getattribute__(name)
        except AttributeError:
            raise

    def __getattr__(self, name):
        """Called if __getattribute__ raises AttributeError"""

    def save_config(self, js_path):
        print('Config.save_config() into [%s]' % js_path)
        with open(js_path, 'w', encoding='utf8') as prpfile:
            prpfile.write(json.dumps(self.prpts, ensure_ascii=False))

    def __getitem__(self, key):
        return self.prpts[key]

    def __setitem__(self, key, value):
        self.prpts[key] = value

    def __contains__(self, key):
        return self.prpts.has_key(key)

    def __str__(self):
        return json.dumps(self.prpts)

    def items(self):
        return self.prpts.items()

    def shear(self, pref=''):
        # returns shear only items contains pref/ ['img_ppt1']:0 -> ['ppt1']:0
        js = {}
        for k, v in self.items():
            if k.startswith(pref):
                dry_key = k.split(pref)[1]
                js[dry_key] = v
        return js

    def has_key(self, key):
        return self.prpts.has_key(key)